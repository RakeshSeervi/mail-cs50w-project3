window.onpopstate = function(event) {
    if(event.state.compose){
        compose_email(event.state.email);
    }
    else if(event.state.email){
        view_mail(event.state.email, event.state.mailbox);
    }
    else{
        load_mailbox(event.state.mailbox);
    }
}


document.addEventListener('DOMContentLoaded', function() {
    
    alertDiv = document.getElementById("alert");

    // Use buttons to toggle between views
    document.querySelector('#inbox').addEventListener('click', function(){
        history.pushState({mailbox:"inbox"}, ``, `#inbox`);
        load_mailbox('inbox');
    });

    document.querySelector('#sent').addEventListener('click', function(){
        history.pushState({mailbox:"sent"}, ``, `#sent`);
        load_mailbox('sent');
    });

    document.querySelector('#archived').addEventListener('click', function(){
        history.pushState({mailbox:"archive"}, ``, `#archived`);
        load_mailbox('archive');
    });

    document.querySelector('#compose').addEventListener('click', function(){
        history.pushState({compose:true}, ``, `#compose`);
        compose_email();
    });
    
    if (window.location.hash){
        if(history.state.compose){
            compose_email(history.state.email);
        }
        else if(history.state.email){
            view_mail(history.state.email, history.state.mailbox);
        }
        else{
            load_mailbox(history.state.mailbox);
        }
    }
    else{
        history.pushState({mailbox:"inbox"}, "", `#inbox`);
        load_mailbox('inbox');
    }
});


function compose_email(mail=null) {
    
    alertDiv.innerHTML="";
    
    // Show compose view and hide other views
    document.querySelector('#emails-view').style.display = 'none';
    document.querySelector('#compose-view').style.display = 'block';
    document.querySelector("#email-view").style.display = "none";
    
   
    document.querySelector("#compose-form").onsubmit = () => {
        
        alertDiv.innerHTML = "";
        
        const recipients = document.querySelector("#compose-recipients").value;
        const subject = document.querySelector("#compose-subject").value;
        const body = document.querySelector("#compose-body").value;
        
        let responseStatus = null;

        if(recipients==""){
            Alert = getAlertElement("Enter atleast one recipient!", "alert-danger");
            alertDiv.appendChild(Alert);
        }
        else if(subject==""){
            Alert = getAlertElement("Subject field cannot be empty!",'alert-danger');
            alertDiv.appendChild(Alert);
        }
        else if(body==""){
            Alert = getAlertElement("Body field cannot be empty!",'alert-danger');
            alertDiv.appendChild(Alert);
        }
        else{
            fetch("/emails", {
                method: 'POST',
                body: JSON.stringify({
                    recipients: recipients,
                    subject: subject,
                    body: body
                })
            })
            .then(function(response) {
                responseStatus = response.status;
                return response.json();
            })
            .then(message => {
                if(responseStatus=='201'){
                    history.pushState({mailbox: "sent"}, "", "#sent");
                    load_mailbox('sent',message.message);
                }
                else{
                    Alert = getAlertElement(message.error, "alert-danger");
                    alertDiv.appendChild(Alert);
                }
            });
        }     
        return false;
    }
    
    // Clear out composition fields
    document.querySelector('#compose-recipients').value = '';
    document.querySelector('#compose-subject').value = '';
    document.querySelector('#compose-body').value = '';

    if(mail){
        document.querySelector('#compose-recipients').value = mail.sender;
        
        if(mail.subject.indexOf("Re:")>-1){
            document.querySelector('#compose-subject').value = mail.subject;
        }
        else{
            document.querySelector('#compose-subject').value = "Re: "+mail.subject;
        }
    
        document.querySelector('#compose-body').value = 'On '+mail.timestamp+", "+mail.sender+" wrote:\n"+mail.body;
    }
}


function load_mailbox(mailbox, message=null) {
    
    alertDiv.innerHTML="";
  
    // Show the mailbox and hide other views
    document.querySelector('#emails-view').style.display = 'block';
    document.querySelector('#compose-view').style.display = 'none';
    document.querySelector("#email-view").style.display = "none";
    old = document.querySelector("#mails");
    old.innerHTML = "";
        
    // Show the mailbox name
    document.querySelector('#heading').innerHTML = `<h3>${mailbox.charAt(0).toUpperCase() + mailbox.slice(1)}</h3>`;

    if(message){
        Alert = getAlertElement(message,'alert-success');
        alertDiv.appendChild(Alert);
    }
    
    fetch(`/emails/${mailbox}`)
    .then(response => response.json())
    .then(emails => {
        emailList = document.createElement("div");
        
        for(let email of emails){
            const sender = document.createElement('b');
            const recipients = document.createElement('span');
        
            if(mailbox=="sent"){
                let rLength = email.recipients.length;
                if(rLength==1){
                    recipients.innerHTML = `To: <b>${email.recipients[0]}</b>`;
                }
                else{
                    recipients.innerHTML = `To: <b>${email.recipients[0]}</b> and ${rLength-1} other(s)`;
                }
            }
            else{
                sender.innerHTML = `${email.sender}`;
            }
            
            const subject = document.createElement('span');
            subject.innerHTML = `${email.subject}`;
            const timestamp = document.createElement('span');
            timestamp.className = "timestamp";
            timestamp.innerHTML = `${email.timestamp}`;
            
            const Email = document.createElement('div');
            
            if(email.read){
                Email.style["background-color"] = "#f4f7f7";
            }
            else{
                Email.style["background-color"] = "white";
            }
            
            if(mailbox=="sent"){
                Email.appendChild(recipients);
                Email.style["background-color"] = "#f4f7f7";
            }
            else{
                Email.appendChild(sender);
            }
            
            Email.appendChild(subject);
            Email.appendChild(timestamp);
            Email.className = "email";
            Email.addEventListener('click', function () {
                history.pushState({email:email.id, mailbox:mailbox}, "", `#${mailbox}/${email.id}`);
                view_mail(email.id, mailbox);
            });
            emailList.appendChild(Email);
            
            old.innerHTML="";
            old.appendChild(emailList);
        }
    });
}


function view_mail(id, mailbox){
    alertDiv.innerHTML = "";
    document.querySelector('#compose-view').style.display = "none";
    document.querySelector('#emails-view').style.display = "none";
    document.querySelector("#email-view").style.display = "block";
    
    const header = document.createElement('table');

    fetch(`/emails/${id}`)
    .then(response => response.json())
    .then(email => {
        let row = document.createElement('tr');
        let heading = document.createElement('th');
        let value = document.createElement('td');

        heading.innerHTML = "From:";
        value.innerHTML = email.sender;
        row.appendChild(heading);
        row.appendChild(value);
        header.appendChild(row);
        
        row = document.createElement("tr");
        heading = document.createElement("th");
        value = document.createElement("td");
        heading.innerHTML = "To:";
        value.innerHTML = email.recipients.join(", ");
        row.innerHTML = heading.outerHTML + value.outerHTML;
        header.appendChild(row);
        
        row = document.createElement("tr");
        heading = document.createElement("th");
        value = document.createElement("td");
        heading.innerHTML = "Subject:";
        value.innerHTML = email.subject;
        row.innerHTML = heading.outerHTML + value.outerHTML;
        header.appendChild(row);
        
        row = document.createElement("tr");
        heading = document.createElement("th");
        value = document.createElement("td");
        heading.innerHTML = "Timestamp:";
        value.innerHTML = email.timestamp;
        row.innerHTML = heading.outerHTML + value.outerHTML;
        header.appendChild(row);
        
        document.querySelector("#email-view").innerHTML = "";
        document.querySelector("#email-view").appendChild(header);
        
        options = document.createElement('div');
        
        if(mailbox!="sent"){
            archiveButton = document.createElement('button');
            archiveButton.className = "btn btn-sm btn-outline-primary";
            
            if(email.archived){
                archiveButton.innerHTML = "Unarchive";
                archiveButton.onclick = () => {
                    unarchive(id);
                }
            }
            else{
                archiveButton.innerHTML = "Archive";
                archiveButton.onclick = function() {
                    archive(id);
                }
            }
            options.appendChild(archiveButton);
        }
        
        replyButton = document.createElement('button');
        replyButton.className = "btn btn-sm btn-outline-primary";
        replyButton.innerHTML = "Reply";
        replyButton.onclick = function(){
            history.pushState({compose:true, email:email}, "", `#compose`);
            compose_email(email);
        }
        
        space = document.createElement('span');
        space.innerHTML = "&nbsp";
        
        options.appendChild(space);
        options.appendChild(replyButton);
        
        document.querySelector("#email-view").appendChild(options);
        
        horizontalLine = document.createElement("hr");
        document.querySelector("#email-view").appendChild(horizontalLine);
    
        body = document.createElement("div");
        body.innerHTML = email.body;
        body.style["white-space"] = "pre-line";
        document.querySelector("#email-view").appendChild(body);
    });

    fetch(`/emails/${id}`,{
        method: 'PUT',
        body: JSON.stringify({
            read: true
        })
    });
              
}


function getAlertElement(message, statusClass){
    
    alertSpan = document.createElement('span');
    alertSpan["aria-hidden"] = "true";
    alertSpan.innerHTML = "&times";

    alertButton = document.createElement('button');
    alertButton.type = 'button';
    alertButton.className = 'close';
    alertButton.setAttribute("data-dismiss", "alert");
    alertButton.setAttribute("aria-label", "Close");
    alertButton.appendChild(alertSpan);

    alertMessage = document.createElement('span');
    alertMessage.innerHTML = message;

    Alert = document.createElement('div');
    Alert.className = 'alert alert-dismissable fade show ';
    Alert.role = "alert";
    Alert.appendChild(alertButton);
    Alert.appendChild(alertMessage);
    
    Alert.className+=statusClass;
    
    return Alert
}


function archive(id){
    fetch(`/emails/${id}`,{
        method: "PUT",
        body: JSON.stringify({
            archived: true
        })
    })
    .then(function(response){
        history.pushState({mailbox:"inbox"}, ``, `#inbox`);
        load_mailbox("inbox");
    });
}
  
  
function unarchive(id){
    fetch(`emails/${id}`,{
          method: 'PUT',
          body: JSON.stringify({
              archived: false
          })
    })
    .then(function(response){
        history.pushState({mailbox:"inbox"}, "", `#inbox`);
        load_mailbox("inbox");
    });
}
