asgiref==3.2.10
Django==3.1
future==0.18.2
pyngrok==4.1.10
pytz==2020.1
PyYAML==5.3.1
sqlparse==0.3.1
